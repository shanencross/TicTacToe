﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TicTacToe {
    static class Opponent {
        static readonly Random rnd = new Random();
        public static int GetInput(Board board) {
            // Win if possible. If not, block if possible.
            int victoryOrBlockSpace = FindVictoryOrBlockSpace(board);

            if (victoryOrBlockSpace > -1) {
                return victoryOrBlockSpace;
            }

            // If unable to win or block, pick a random sapce.
            return FindRandomOpenSpace(board);
        }

        static int FindRandomOpenSpace(Board board) {
            List<int> openSpaces = new List<int>();

            for (int i = 1; i <= board.size; i++) {
                if (board.GetSpace(i) == Board.Space.empty) {
                    openSpaces.Add(i);
                }
            }

            if (openSpaces.Count == 0) {
                Console.WriteLine("No open spaces.");
                return -1;
            }

            int randomOpenSpaceIndex = rnd.Next(openSpaces.Count);
            int randomOpenSpace = openSpaces[randomOpenSpaceIndex];

            return randomOpenSpace;
        }

        static int FindVictoryOrBlockSpace(Board board) {
            List<Board.Space> victoryPattern1 = new List<Board.Space> { Board.Space.o, Board.Space.o, Board.Space.empty };
            List<Board.Space> victoryPattern2 = new List<Board.Space> { Board.Space.o, Board.Space.empty, Board.Space.o };
            List<Board.Space> victoryPattern3 = new List<Board.Space> { Board.Space.empty, Board.Space.o, Board.Space.o };

            List<Board.Space> blockPattern1 = new List<Board.Space> { Board.Space.x, Board.Space.x, Board.Space.empty };
            List<Board.Space> blockPattern2 = new List<Board.Space> { Board.Space.x, Board.Space.empty, Board.Space.x };
            List<Board.Space> blockPattern3 = new List<Board.Space> { Board.Space.empty, Board.Space.x, Board.Space.x };

            List<List<Board.Space>> patternList = new List<List<Board.Space>> {
                victoryPattern1,
                victoryPattern2,
                victoryPattern3,
                blockPattern1,
                blockPattern2,
                blockPattern3
            };

            foreach (List<Board.Space> pattern in patternList) {
                int index = FindVictoryOrBlockSpaceFromPattern(board, pattern, out int[] matchedIndices);
                if (index > -1) {
                    return index;
                }
            }

            return -1;
        }

        static int FindVictoryOrBlockSpaceFromPattern(Board board, List<Board.Space> pattern, out int[] matchedIndices) {
            if (board.FindPattern(pattern, out matchedIndices)) {
                foreach (int index in matchedIndices) {
                    if (board.GetSpace(index) == Board.Space.empty) {
                        return index;
                    }
                }
            }

            return -1;
        }
    }
}
