﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TicTacToe {
    class Board {
        public enum Space { x, o, empty };
        public readonly int size = 9;
        Space[] spaces;

        public Board() {
            spaces = new Space[size];

            for (int i = 0; i < spaces.Length; i++) {
                spaces[i] = Space.empty;
            }
        }

        public bool ValidateMove(int index) {
            if (index < 1 || index > size || spaces[index - 1] != Space.empty) {
                return false;
            }

            return true;
        }

        public Space GetSpace(int index) {
            if (index < 1 || index > size) {
                Console.WriteLine("Invalid space index passed to GetSpace().");
                return Space.empty;
            }

            return spaces[index - 1];
        }

        public void Move(string player, int index) {
            if (player != "x" && player != "o") {
                Console.WriteLine("Invalid player string passed to Move().");
                return;
            }

            if (!ValidateMove(index)) {
                Console.WriteLine("Invalid index passed to Move().");
                return;
            }

            if (player == "x") {
                spaces[index - 1] = Space.x;
            }
            else if (player == "o") {
                spaces[index - 1] = Space.o;
            }

            Console.WriteLine($"{player.ToUpper()} played space {index}.");
        }

        public void Display() {
            string displayOutput = "";
            for (int i = 0; i < spaces.Length; i++) {
                Space space = spaces[i];

                if (space == Space.x) {
                    displayOutput += "x";
                }
                else if (space == Space.o) {
                    displayOutput += "o";
                }
                else if (space == Space.empty) {
                    displayOutput += "*";
                }

                if (i == 2 || i == 5) {
                    displayOutput += "\n";
                }
                else if (i != 8) {
                    displayOutput += " | ";
                }
            }

            Console.WriteLine(displayOutput);
        }

        public bool IsGameOver() {
            List<Space> xVictoryPattern = new List<Space> { Space.x, Space.x, Space.x };
            List<Space> oVictoryPattern = new List<Space> { Space.o, Space.o, Space.o };
            int[] matchedIndices;
            if (FindPattern(xVictoryPattern, out matchedIndices)) {
                Console.WriteLine("X is the winner!");
                return true;
            }
            else if (FindPattern(oVictoryPattern, out matchedIndices)) {
                Console.WriteLine("O is the winner!");
                return true;
            }

            if (!spaces.Contains(Space.empty)) {
                Console.WriteLine("It's a tie!");
                return true;
            }

            return false;
        }

        public bool FindPattern(List<Space> pattern, out int[] matchedIndices) {
            return FindHorizontalPattern(pattern, out matchedIndices) ||
                   FindVerticalPattern(pattern, out matchedIndices) ||
                   FindDiagonalPattern(pattern, out matchedIndices);
        }

        bool FindHorizontalPattern(List<Space> pattern, out int[] matchedIndices) {
            return MatchPattern(pattern, out matchedIndices, 1, 2, 3) ||
                   MatchPattern(pattern, out matchedIndices, 4, 5, 6) ||
                   MatchPattern(pattern, out matchedIndices, 7, 8, 9);
        }

        bool FindVerticalPattern(List<Space> pattern, out int[] matchedIndices) {
            return MatchPattern(pattern, out matchedIndices, 1, 4, 7) ||
                   MatchPattern(pattern, out matchedIndices, 2, 5, 8) ||
                   MatchPattern(pattern, out matchedIndices, 3, 6, 9);
        }

        bool FindDiagonalPattern(List<Space> pattern, out int[] matchedIndices) {
            return MatchPattern(pattern, out matchedIndices, 1, 5, 9) ||
                   MatchPattern(pattern, out matchedIndices, 3, 5, 7);
        }

        bool MatchPattern(List<Space> pattern, out int[] matchedIndices, params int[] indices) {
            List<Space> patternCheck = new List<Space>();
            foreach (int index in indices) {
                patternCheck.Add(GetSpace(index));
            }


            bool matched = patternCheck.SequenceEqual(pattern);

            if (matched) {
                matchedIndices = indices;
            }
            else {
                matchedIndices = new int[0];
            }

            return matched;
        }

        public void Reset() {
            for (int i = 0; i < spaces.Length; i++) {
                spaces[i] = Space.empty;
            }
        }
    }
}
