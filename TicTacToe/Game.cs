﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TicTacToe {
    class Game {
        bool gameIsRunning = true;
        Board board;

        enum Turn { player, opponent };

        Turn turn = Turn.player;

        public Game() {
            board = new Board();
        }

        public void RunGame() {
            PrintInstructions();
            RunGameLoop();

            Console.WriteLine("Goodbye! (Press any key to exit)");
            Console.ReadKey();
        }

        void RunGameLoop() {
            board.Display();
            Console.WriteLine();

            while (gameIsRunning) {
                if (turn == Turn.player) {
                    DoPlayerMove();
                    turn = Turn.opponent;
                }
                else if (turn == Turn.opponent) {
                    DoOpponentMove();
                    turn = Turn.player;
                }

                if (board.IsGameOver()) {
                    board.Reset();
                    turn = Turn.player;
                    gameIsRunning = AskToContinue();
                }
            }
        }

        void DoPlayerMove() {
            int userInput = GetUserInput();
            Console.WriteLine();

            board.Move("x", userInput);
            board.Display();
            Console.WriteLine();
        }

        void DoOpponentMove() {
            int opponentInput = Opponent.GetInput(board);
            board.Move("o", opponentInput);
            board.Display();
            Console.WriteLine();
        }

        int GetUserInput() {
            while (true) {
                Console.WriteLine("Enter your move (1-9):");
                if (!Int32.TryParse(Console.ReadLine(), out int input)) {
                    Console.WriteLine("Invalid input.");
                    continue;
                }

                if (!board.ValidateMove(input)) {
                    Console.WriteLine("Invalid move.");
                    continue;
                }

                return input;
            }
        }

        void PrintInstructions() {
            Console.WriteLine("Welcome to TicTacToe!");
            Console.WriteLine("You are X! Your computer opponent is O!");
            Console.WriteLine("Moves are placed on the grid with numbers 1-9, as show:");
            Console.WriteLine("1 2 3");
            Console.WriteLine("4 5 6");
            Console.WriteLine("7 8 9");
            Console.WriteLine("Try to line up three Xs before your opponent lines up three Os.");
            Console.WriteLine("Good luck!\n");
        }

        bool AskToContinue() {
            while (true) {
                Console.WriteLine("Do you want to play again? (y/n)");

                string input = Console.ReadLine().ToLower();

                if (input == "y" || input == "yes") {
                    return true;
                }

                if (input == "n" || input == "no") {
                    return false;
                }

                else {
                    Console.WriteLine("Invalid input.");
                }
            }
        }
    }
}
