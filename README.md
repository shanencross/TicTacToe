# TicTacToe

A Tic-tac-toe game for the command line with a simple computer opponent, made with C# in .NET Framework 4.6.1.

To compile/run:

* In Visual Studio (tested with VS2017 on Windows), install the ".NET Framework 4.6.1 Development Tools", which are included in the ".NET Desktop Development" workload in the Visual Studio installer.

* Open TicTacToe.sln in Visual Studio and build/run with "Play".

Visual Studio uses MSBuild (Microsoft Build Tools) in conjunction with the Roslyn C# compiler. You may be able to build this without Visual Studios if you acquire either of these separately, but I haven't tested this.

